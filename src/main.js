import Vue from 'vue'
import App from './App.vue'
import vuetify from './store/vuetify'
import router from './router/index'
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
